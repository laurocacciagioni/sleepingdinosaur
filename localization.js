var userLang = navigator.language || navigator.userLanguage;
console.log(userLang);
var indexAPP = angular.module('index', []);
indexAPP.controller('indexController', function ($scope) {
    if(userLang.includes("es"))
    {
        $scope.home = "Inicio";

        $scope.contact ="Contacto";
        $scope.social ="Redes Sociales";
       
        $scope.name="Nombre";
        $scope.mail="E-Mail";
        $scope.message="Mensaje";
        $scope.send="Enviar";
        $scope.message_sent="Mensaje enviado";
        $scope.enter_message="Ingrese su mensaje";
        $scope.enter_mail="Ingrese su E-Mail";
        $scope.enter_name="Ingrese su nombre";
        $scope.subject ="Asunto";
        $scope.imagen ="Click en la imagen para ir al sitio"
    }
    else
    {
        $scope.home = "Home";

        $scope.contact="Contact";
        $scope.social="Social";
        
        $scope.name="Name";
        $scope.mail="E-Mail";
        $scope.message="Message";
        $scope.send="Send";
        $scope.message_sent="Message sent";
        $scope.enter_message="Enter your message";
        $scope.enter_mail="Enter your E-Mail";
        $scope.enter_name="Enter your name";
        $scope.subject="Subject";
        $scope.imagen ="Click on the image to go to the site"
    }
});